class grid:
    BLANK = ' '
    def __init__(self, row: list[list[int]], column: list[list[int]]) -> None:
        self.rows = row
        self.columns = column
    def __repr__(self):
        return ((len(self.rows) * ' ') + '\n') * len(self.columns)

class picture:
    SHARP = '#'
    STAR = '*'
    definite = [[5],[1,3],[3,1],[2,2]]
    def __init__(self):
        self.x = None
    def change(self, clue):
        if clue in definite:
